let parser = new DOMParser();
let xmlString = `
<universidad>
  <facultad nombre="Ingeniería">
    <curso nombre="Programación">
      <estudiante>
        <nombre>Jahir</nombre>
        <apellido>Pacheco</apellido>
        <edad>22</edad>
      </estudiante>
      <estudiante>
        <nombre>Romina</nombre>
        <apellido>García</apellido>
        <edad>20</edad>
      </estudiante>
    </curso>
    <curso nombre="Fisica">
      <estudiante>
        <nombre>Luis</nombre>
        <apellido>Rodríguez</apellido>
        <edad>21</edad>
      </estudiante>
    </curso>
  </facultad>
  <facultad nombre="Ciencias">
    <curso nombre="Biología">
      <estudiante>
        <nombre>Carlos</nombre>
        <apellido>Martínez</apellido>
        <edad>23</edad>
      </estudiante>
    </curso>
  </facultad>
</universidad>
`;
let xmlDoc = parser.parseFromString(xmlString, "text/xml");

console.log(xmlDoc);
