let universidad = {
  facultades: [
    {
      nombre: "Ingeniería",
      cursos: [
        {
          nombre: "Programación",
          estudiantes: [
            {
              nombre: "Jahir",
              apellido: "Pacheco",
              edad: 22
            },
            {
              nombre: "Romina",
              apellido: "García",
              edad: 20
            }
          ]
        },
        {
          nombre: "Fisica",
          estudiantes: [
            {
              nombre: "Luis",
              apellido: "Rodríguez",
              edad: 21
            }
          ]
        }
      ]
    },
    {
      nombre: "Ciencias",
      cursos: [
        {
          nombre: "Biología",
          estudiantes: [
            {
              nombre: "Carlos",
              apellido: "Martínez",
              edad: 23
            }
          ]
        }
      ]
    }
  ]
};

console.log(universidad);
